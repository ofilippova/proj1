require: slotfilling/slotFilling.sc
  module = sys.zb-common
  
require: patterns.sc

theme: /

    state: Start
        q!: $regex</start>
        a: Начнём.

    sta1te: Hello
        intent!: /привет
        a: Привет привет

    state: Bye
        intent!: /пока
        a: Пока пока

    state: NoMatch
        event!: noMatch
        a: Я не понял. Вы сказали: {{$request.query}}

